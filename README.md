# Arcadia Launcher

> Desktop application to assist with vagrant project management

## Get started

```
$ npm install && npm install -g electron
```

### Run

```
$ npm start
```

## License

The MIT License (MIT) © Shane Shipston 2018
